import React, { Component } from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import UserBox from './UserBox';
import Loader from '../../components/Loader';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.onUpdateDmatData = this.onUpdateDmatData.bind(this);
    this.onAddDmatData = this.onAddDmatData.bind(this);
    this.onDeleteDmatData = this.onDeleteDmatData.bind(this);
  }

  async onUpdateDmatData(data, index) {
    const { firestore, userData, uid } = this.props;
    const { brokerIds } = userData;
    let tempArray = [...brokerIds] || [];
    tempArray[index] = {
      ...tempArray[index],
      ...data,
    };
    const params = {
      brokerIds: tempArray,
    };
    const res = await firestore.collection('users').doc(uid).set(params, { merge: true });
  }

  async onAddDmatData(data) {
    const { firestore, userData, uid } = this.props;
    const { brokerIds } = userData;
    let tempArray = [...brokerIds] || [];
    tempArray.push({
      brokerId: 'zebu',
      userUid: uid,
      ...data,
    });
    const params = {
      brokerIds: tempArray,
    };
    const res = await firestore.collection('users').doc(uid).set(params, { merge: true });
  }

  async onDeleteDmatData(index) {
    const { firestore, userData, uid } = this.props;
    const { brokerIds } = userData;
    let tempArray = [...brokerIds] || [];
    tempArray.splice(index, 1);

    const params = {
      brokerIds: tempArray,
    };
    const res = await firestore.collection('users').doc(uid).set(params, { merge: true });
  }

  render() {
    if (!this.props.userData) {
      return <Loader />;
    }
    return (
      <React.Fragment>
        <Row className='page-title'>
          <Col md={12}>
            <h4 className='mb-1 mt-0'>Profile</h4>
          </Col>
        </Row>

        <Row>
          <Col lg={12}>
            <UserBox
              userData={this.props.userData}
              onUpdateDmatData={this.onUpdateDmatData}
              onAddDmatData={this.onAddDmatData}
              onDeleteDmatData={this.onDeleteDmatData}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const withFireStoreWrapper = compose(
  firestoreConnect(['users']),
  connect((state, props) => {
    return {
      userData: state.firestore.data.users ? state.firestore.data.users[props.uid] || null : null,
    };
  })
);

export default withFireStoreWrapper(Profile);
