// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Loader from '../../components/Loader';
import SideBarContainer from '../DashBoard/SideBarContainer';
import ProfileComponent from './Profile';

class Profile extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.auth) {
      return <Loader />;
    }
    return (
      <SideBarContainer active_id='myprofile'>
        <React.Fragment>
          <ProfileComponent {...this.props} uid={this.props.auth.uid} />
        </React.Fragment>
      </SideBarContainer>
    );
  }
}

const withFireStoreWrapper = compose(
  connect((state) => ({
    auth: state.firebase.auth,
  }))
);

export default withFireStoreWrapper(Profile);
