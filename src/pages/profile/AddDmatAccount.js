// @flow
import React from 'react';
import { Row, Col, Card, CardBody, UncontrolledPopover, PopoverBody, Button } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';

const AddDmatAccount = ({ onAddDmatData }) => {
  const [dmatId, setDmatId] = React.useState('');
  const [dmatApiKey, setDmatApiKey] = React.useState('');

  return (
    <Col lg={4}>
      <Card className='' style={{ minHeight: 320 }}>
        <CardBody className='profile-user-box '>
          <AvForm className='needs-validation mt-2' noValidate name='chat-form' id='chat-form'>
            <Row form>
              <Col>
                <AvField
                  onChange={(e) => {
                    setDmatId(e.target.value);
                  }}
                  name='dmatId'
                  label='Dmat Id'
                  type='text'
                  val
                  value={dmatId}
                />
                <AvField
                  name='apiKey'
                  label='Api Key'
                  type='text'
                  onChange={(e) => {
                    setDmatApiKey(e.target.value);
                  }}
                  value={dmatApiKey}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  disabled={!(dmatId && dmatApiKey)}
                  color='primary'
                  type='submit'
                  onClick={() => {
                    if (dmatId && dmatApiKey) {
                      let date = new Date();
                      date.setDate(date.getDate() + 30);
                      onAddDmatData({
                        userid: dmatId,
                        apiKey: dmatApiKey,
                        apiKeyExpiryDate: date,
                      });
                      setDmatId('');
                      setDmatApiKey('');
                    }
                  }}
                >
                  Add Dmat account
                </Button>
              </Col>
            </Row>
          </AvForm>
        </CardBody>
      </Card>
    </Col>
  );
};

export default AddDmatAccount;
