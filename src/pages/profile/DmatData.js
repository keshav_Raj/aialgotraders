// @flow
import React from 'react';
import { Row, Col, Card, CardBody, UncontrolledPopover, PopoverBody, Button } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import classNames from 'classnames';

const DmatData = ({ dmatData, onUpdateDmatData, onDeleteDmatData, index }) => {
  const [isEditClicked, setIsEditClicked] = React.useState(false);
  const [dmatId, setDmatId] = React.useState(dmatData.userid);
  const [dmatApiKey, setDmatApiKey] = React.useState(dmatData.apiKey);

  const handleValidMessageSubmit = () => null;
  if (isEditClicked) {
    return (
      <Col lg={4}>
        <Card className='' style={{ minHeight: 320 }}>
          <CardBody className='profile-user-box '>
            <AvForm
              onValidSubmit={handleValidMessageSubmit}
              className='needs-validation mt-2'
              noValidate
              name='chat-form'
              id='chat-form'
            >
              <Row form>
                <Col>
                  <AvField
                    onChange={(e) => {
                      setDmatId(e.target.value);
                    }}
                    name='dmatId'
                    label='Dmat Id'
                    type='text'
                    required
                    val
                    value={dmatId}
                  />
                  <AvField
                    name='apiKey'
                    label='Api Key'
                    type='text'
                    required
                    onChange={(e) => {
                      setDmatApiKey(e.target.value);
                    }}
                    value={dmatApiKey}
                  />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Button
                    disabled={!(dmatId && dmatApiKey)}
                    color='primary'
                    type='submit'
                    onClick={() => {
                      if (dmatId && dmatApiKey) {
                        let date = new Date();
                        date.setDate(date.getDate() + 30);
                        onUpdateDmatData(
                          {
                            ...dmatData,
                            userid: dmatId,
                            apiKey: dmatApiKey,
                            apiKeyExpiryDate: date,
                          },
                          index
                        );
                        setIsEditClicked(false);
                      }
                    }}
                  >
                    Update
                  </Button>
                </Col>
              </Row>
            </AvForm>
          </CardBody>
        </Card>
      </Col>
    );
  }
  return (
    <Col lg={4}>
      <Card className='' style={{ minHeight: 320 }}>
        <CardBody className='profile-user-box'>
          <Row>
            <Col>
              <div className='mt-1 pt-2'>
                <Row className='page-title align-items-center'>
                  <Col sm={4}>
                    <h4 className='mb-1 mt-0'>DMAT</h4>
                  </Col>
                </Row>

                <div className='table-responsive'>
                  <table className='table table-borderless mb-0 text-muted'>
                    <tbody>
                      <tr>
                        <th scope='row'>Id</th>
                        <td>{dmatId}</td>
                      </tr>
                      <tr>
                        <th scope='row'>Api key</th>
                        <td>{dmatApiKey}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col xl={4} />
            <Col xl={8}>
              <div className='d-flex mr-3'>
                <button
                  onClick={() => {
                    setIsEditClicked(true);
                  }}
                  className='btn btn-warning chat-send btn-block'
                >
                  Edit
                </button>
                <div className='mr-1' />
                <React.Fragment>
                  <Button className='mr-1' color='danger' type='button' id={`deleteDmat-${dmatId}`}>
                    Delete
                  </Button>
                  <UncontrolledPopover
                    placement='top'
                    id={`deleteDmat-${dmatId}`}
                    target={`deleteDmat-${dmatId}`}
                    trigger='click'
                  >
                    <PopoverBody>
                      <button
                        onClick={() => {
                          onDeleteDmatData(index);
                        }}
                        className='btn btn-info chat-send btn-block'
                      >
                        Confirm delete
                      </button>
                    </PopoverBody>
                  </UncontrolledPopover>
                </React.Fragment>
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </Col>
  );
};

export default DmatData;
