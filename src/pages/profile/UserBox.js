import React from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';
import profileImg from '../../assets/images/users/avatar-7.png';
import DmatData from './DmatData';
import AddDmatAccount from './AddDmatAccount';

const UserBox = ({ userData, onUpdateDmatData, onAddDmatData, onDeleteDmatData }) => {
  return (
    <>
      <Row>
        <Col lg={4}>
          <Card className='' style={{ minHeight: 320 }}>
            <CardBody className='profile-user-box'>
              <Row>
                <Col>
                  <div className='text-center mt-3'>
                    <img src={profileImg} alt='' className='avatar-lg rounded-circle' />
                    <h5 className='mt-2 mb-0'>{userData.name}</h5>
                  </div>

                  <div className='mt-3 pt-2 border-top'>
                    <h4 className='mb-3 font-size-15'>Contact Information</h4>
                    <div className='table-responsive'>
                      <table className='table table-borderless mb-0 text-muted'>
                        <tbody>
                          <tr>
                            <th scope='row'>Email</th>
                            <td>{userData.email}</td>
                          </tr>
                          <tr>
                            <th scope='row'>Phone</th>
                            <td>{userData.phoneNumber}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
        {(userData.brokerIds || []).map((broker, index) => {
          return (
            <DmatData
              index={index}
              dmatData={broker}
              onUpdateDmatData={onUpdateDmatData}
              onDeleteDmatData={onDeleteDmatData}
            />
          );
        })}
        <AddDmatAccount onAddDmatData={onAddDmatData} />
      </Row>
    </>
  );
};

export default UserBox;
