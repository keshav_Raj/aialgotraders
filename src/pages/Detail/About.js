import React from 'react';
import { Card, CardBody } from 'reactstrap';

const About = ({ longDescription }) => {
  return (
    <React.Fragment>
      <Card>
        <CardBody>
          <h6 className='mt-0 header-title'>Description</h6>
          <div className='text-muted mt-3'>
            <p>{longDescription}</p>
          </div>
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

export default About;
