// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import Loader from '../../components/Loader';
import SideBarContainer from '../DashBoard/SideBarContainer';
import StratergyDetail from './StratergyDetail';

class Detail extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.strategy) {
      return <Loader />;
    }
    return (
      <SideBarContainer active_id='marketPlace'>
        <React.Fragment>
          <StratergyDetail {...this.props} />
        </React.Fragment>
      </SideBarContainer>
    );
  }
}

const withFireStoreWrapper = compose(
  firestoreConnect(['strategies']),
  connect((state, props) => {
    const strategyId = props?.match?.params?.id || 0;
    return {
      strategy: state.firestore.data.strategies?.[strategyId] || null,
      strategyId: strategyId,
    };
  })
);

export default withFireStoreWrapper(Detail);
