// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import Loader from '../../components/Loader';
import SideBarContainer from '../DashBoard/SideBarContainer';
import StratergyDetail from './StratergyDetail';

class SubscribedStratergyDetails extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { strategies, strategy, trades } = this.props;
    if (!(strategies && strategy)) {
      return <Loader />;
    }
    return (
      <SideBarContainer active_id='mystratergies'>
        <React.Fragment>
          <StratergyDetail {...this.props} fromMyStratergy />
        </React.Fragment>
      </SideBarContainer>
    );
  }
}

const withFireStoreWrapper = compose(
  firestoreConnect((props) => {
    const strategyId = props?.match?.params?.id || 0;
    return [
      {
        collection: 'Trades',
        where: ['Strategy ID', '==', strategyId],
      },
      {
        collection: 'strategies',
      },
    ];
  }),
  connect((state, props) => {
    const strategyId = props?.match?.params?.id || 0;
    return {
      trades: state.firestore.data.Trades || null,
      strategy: state.firestore.data.strategies?.[strategyId] || null,
      strategies: state.firestore.data.strategies || null,
    };
  })
);

export default withFireStoreWrapper(SubscribedStratergyDetails);
