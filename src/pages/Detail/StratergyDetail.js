import React, { Component } from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';
import ProjectStats from './Stats';
import About from './About';
import BackTestResultTable from '../DashBoard/TableContainer';
import LineChart from './LineChart';
import TradesTable from '../DashBoard/TradesTable';

class StratergyDetail extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { strategy, fromMyStratergy, strategyId, trades, strategies } = this.props;
    const { backtestHighlights, title, longDescription } = strategy;
    const showTable = !fromMyStratergy ? strategyId && strategy : trades && strategies;
    return (
      <React.Fragment>
        <Row className='page-title'>
          <Col sm={8} xl={6}>
            <h4 className='mb-1 mt-0'>{`Stratergy: ${title}`}</h4>
          </Col>
        </Row>

        <ProjectStats backtestHighlights={backtestHighlights} />

        <Row>
          <Col xl={12}>
            <About longDescription={longDescription} />
            <LineChart />
            {showTable && (
              <Card>
                <CardBody>
                  {fromMyStratergy ? (
                    <TradesTable trades={trades} strategies={strategies} />
                  ) : (
                    <BackTestResultTable strategyId={strategyId} strategy={strategy} />
                  )}
                </CardBody>
              </Card>
            )}
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default StratergyDetail;
