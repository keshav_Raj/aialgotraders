import React from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';
import Statistics from '../DashBoard/Statistics';

const ProjectStats = (props) => {
  return (
    <React.Fragment>
      <Row>
        <Col>
          <Card>
            <CardBody className='p-0'>
              <h6 className='card-title border-bottom p-3 mb-0 header-title'>Highlights</h6>
              <div className='p-3'>
                <Statistics backtestHighlights={props.backtestHighlights} />
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default ProjectStats;
