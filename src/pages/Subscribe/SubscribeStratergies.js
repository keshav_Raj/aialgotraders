// @flow
import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import Loader from '../../components/Loader';
import SideBarContainer from '../DashBoard/SideBarContainer';
import Wizard from './Wizard';

class SubscribeStratergies extends Component {
  constructor(props) {
    super(props);
    this.deployStratergy = this.deployStratergy.bind(this);
  }

  async deployStratergy(data) {
    const { firestore, userData, userId, stratergyId, strategyMapping } = this.props;

    if (!strategyMapping) {
      return;
    }
    const { brokerIds = [] } = userData;
    const { demataccount } = strategyMapping;

    let tempArray = [...demataccount] || [];
    brokerIds.forEach((dmatData) => {
      tempArray.push({
        apiKey: dmatData.apiKey,
        telegramid: dmatData.telegramid || '',
        userid: dmatData.userid,
        userUid: userId,
        eachstockmarginquantity: '-1',
        quantity: data.quantity,
      });
    });
    const params = {
      demataccount: tempArray,
    };
    const res = await firestore
      .collection('userStrategyMappings')
      .doc(stratergyId)
      .set(params, { merge: true });
  }

  render() {
    if (!this.props.strategy || !this.props.strategyMapping) {
      return <Loader />;
    }
    return (
      <SideBarContainer active_id='marketPlace'>
        <React.Fragment>
          <Row className='page-title'>
            <Col sm={8} xl={6}>
              <h4 className='mb-1 mt-0'>{this.props.strategy.title}</h4>
            </Col>
          </Row>
          <Row>
            <Col xl={8}>
              <Wizard {...this.props} deployStratergy={this.deployStratergy} />
            </Col>
          </Row>
        </React.Fragment>
      </SideBarContainer>
    );
  }
}

const withFireStoreWrapper = compose(
  firestoreConnect(['strategies', 'userStrategyMappings']),
  connect((state, props) => {
    const stratergyId = props?.match?.params?.id || 0;
    const userId = state.firebase.auth.uid;
    return {
      strategy: state.firestore.data.strategies?.[stratergyId] || null,
      strategyMapping: state.firestore.data.userStrategyMappings?.[stratergyId] || null,
      userData: state.firestore.data.users ? state.firestore.data.users[userId] || null : null,
      userId: userId,
      stratergyId: stratergyId,
    };
  })
);

export default withFireStoreWrapper(SubscribeStratergies);
