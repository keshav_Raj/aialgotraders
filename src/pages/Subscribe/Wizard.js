import React from 'react';
import {
  Row,
  Col,
  Card,
  CardBody,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  CustomInput,
  Progress,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { AvForm, AvField, AvInput } from 'availity-reactstrap-validation';
import { Wizard, Steps, Step } from 'react-albus';
import { InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';

const WizardWithProgressbar = ({ deployStratergy, strategy }) => {
  const { title, shortDescription, minQty, qtyIncrementValue } = strategy;
  const [currentQuantity, setCurrentQuantity] = React.useState(Number(minQty));
  const [isAcceptedTerm, setIsAcceptedTerm] = React.useState(false);
  const onClickIncrement = () => {
    setCurrentQuantity(Number(currentQuantity) + Number(qtyIncrementValue));
  };
  const onClickDecrement = () => {
    if (Number(currentQuantity) - Number(qtyIncrementValue) >= minQty) {
      setCurrentQuantity(Number(currentQuantity) - Number(qtyIncrementValue));
    }
  };
  const disableDecrementBtn = Number(currentQuantity) - Number(qtyIncrementValue) < Number(minQty);

  return (
    <Card>
      <CardBody>
        <p className='sub-header'>{shortDescription}</p>

        <Wizard
          render={({ step, steps }) => (
            <React.Fragment>
              <Progress
                animated
                striped
                color='success'
                value={((steps.indexOf(step) + 1) / steps.length) * 100}
                className='mb-3 progress-sm'
              />

              <Steps>
                <Step
                  id='selectQuantity'
                  render={({ next }) => (
                    <Row>
                      <Col sm={12}>
                        <div className=''>
                          <h2 className='mt-0'>
                            <i className='mdi mdi-check-all'></i>
                          </h2>
                          <h3 className='mt-0'>Please provide the following data</h3>

                          <div className='mb-3'>
                            <AvForm
                              onValidSubmit={(event, values) => {
                                next();
                              }}
                            >
                              <div className='mt-3' style={{ maxWidth: '300px' }}>
                                <FormGroup>
                                  <Label>
                                    <h5 className='mt-0'>Quantity:</h5>
                                  </Label>
                                  <InputGroup>
                                    <InputGroupAddon addonType='prepend'>
                                      <Button
                                        disabled={disableDecrementBtn}
                                        onClick={onClickDecrement}
                                        color='primary'
                                        className='btn-rounded width-sm'
                                        size='sm'
                                      >
                                        <h5 className='m-0 font-size-lg text-white'>-</h5>
                                      </Button>
                                    </InputGroupAddon>
                                    <Input
                                      type='text'
                                      name='text'
                                      id='text1'
                                      placeholder='Quantity'
                                      value={currentQuantity}
                                      readOnly
                                    />

                                    <InputGroupAddon addonType='append'>
                                      <Button
                                        onClick={onClickIncrement}
                                        color='primary'
                                        className='btn-rounded width-sm'
                                        size='sm'
                                      >
                                        <h5 className='m-0 font-size-lg text-white'>+</h5>
                                      </Button>
                                    </InputGroupAddon>
                                  </InputGroup>
                                </FormGroup>
                              </div>

                              <ul className='list-inline wizard mb-0'>
                                <li className='next list-inline-item float-right'>
                                  <Button color='success' type='submit'>
                                    Preview
                                  </Button>
                                </li>
                              </ul>
                            </AvForm>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  )}
                />
                <Step
                  id='preview'
                  render={({ next, previous }) => (
                    <Row>
                      <Col sm={12}>
                        <div className=''>
                          <h2 className='mt-0'>
                            <i className='mdi mdi-check-all'></i>
                          </h2>
                          <p className='w-75 mb-2 '>Selected Quantity is {currentQuantity}</p>
                          <p className='w-75 mb-2 '>Each stock margin Quantity : -1</p>
                          <div className='mb-3'>
                            <CustomInput
                              onChange={() => {
                                setIsAcceptedTerm(!isAcceptedTerm);
                              }}
                              type='checkbox'
                              id='deployCheckBoxRefId'
                              label='I agree with the Terms and Conditions'
                            />
                          </div>
                        </div>
                      </Col>

                      <Col sm={12}>
                        <ul className='list-inline wizard mb-0'>
                          <li className='previous list-inline-item'>
                            <Button onClick={previous} color='info'>
                              Previous
                            </Button>
                          </li>

                          <li className='next list-inline-item float-right'>
                            <Button
                              onClick={() => {
                                deployStratergy({
                                  quantity: currentQuantity,
                                });
                                next();
                              }}
                              disabled={!isAcceptedTerm}
                              color='success'
                            >
                              Submit
                            </Button>
                          </li>
                        </ul>
                      </Col>
                    </Row>
                  )}
                />
                <Step
                  id='success'
                  render={({ previous }) => (
                    <Row>
                      <Col sm={12}>
                        <div className='text-center'>
                          <h2 className='mt-0'>
                            <i className='mdi mdi-check-all'></i>
                          </h2>
                          <h3 className='mt-0'>Thank you!</h3>

                          <p className='w-75 mb-2 mx-auto'>
                            {title} is successfully deployed, Our Backend Team will review and
                            approve
                          </p>
                        </div>
                      </Col>

                      <Col sm={12}>
                        <ul className='list-inline wizard mb-0'>
                          <li className='next list-inline-item float-right'>
                            <Link to={`/stratergies`}>
                              <Button color='success'>Go to mystratergies</Button>
                            </Link>
                          </li>
                        </ul>
                      </Col>
                    </Row>
                  )}
                />
              </Steps>
            </React.Fragment>
          )}
        />
      </CardBody>
    </Card>
  );
};
export default WizardWithProgressbar;
