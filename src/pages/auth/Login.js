import React, { Component } from 'react';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Label,
  FormGroup,
  Button,
  Alert,
  InputGroup,
  InputGroupAddon,
} from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';
import { Mail, Lock } from 'react-feather';
import Loader from '../../components/Loader';
import logo from '../../assets/images/logo.png';
import { withFirebase } from 'react-redux-firebase';

class Login extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);

    this.handleValidSubmit = this.handleValidSubmit.bind(this);
    this.state = {
      username: '',
      password: '',
      loading: false,
    };
  }

  componentDidMount() {
    this._isMounted = true;
    document.body.classList.add('authentication-bg');
  }

  componentWillUnmount() {
    this._isMounted = false;
    document.body.classList.remove('authentication-bg');
  }

  handleValidSubmit = (event, values) => {
    this.setState({ loading: true });
    this.props.firebase
      .login({
        email: values.username,
        password: values.password,
      })
      .then(() => {
        this.setState({ loading: false });
      })
      .catch(() => {
        this.setState({ loading: false });
      });
  };

  render() {
    return (
      <React.Fragment>
        <div className='account-pages my-5'>
          <Container>
            <Row className='justify-content-center'>
              <Col xl={10}>
                <Card className=''>
                  <CardBody className='p-0'>
                    <Row>
                      <Col md={6} className='p-5 position-relative'>
                        {/* preloader */}
                        {this.props.loading && <Loader />}

                        <div className='mx-auto mb-5'>
                          <a href='/'>
                            <img src={logo} alt='' height='24' />
                            <h3 className='d-inline align-middle ml-1 text-logo'>AiAlgoTrader</h3>
                          </a>
                        </div>

                        <h6 className='h5 mb-0 mt-4'>Welcome back!</h6>
                        <p className='text-muted mt-1 mb-4'>
                          Enter your email address and password to access panel.
                        </p>

                        {this.props.error && (
                          <Alert color='danger' isOpen={this.props.error ? true : false}>
                            <div>{this.props.error}</div>
                          </Alert>
                        )}

                        <AvForm
                          onValidSubmit={this.handleValidSubmit}
                          className='authentication-form'
                        >
                          <AvGroup className=''>
                            <Label for='username'>Username</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <span className='input-group-text'>
                                  <Mail className='icon-dual' />
                                </span>
                              </InputGroupAddon>
                              <AvInput
                                type='text'
                                name='username'
                                id='username'
                                placeholder='abc@gmail.com'
                                value={this.state.username}
                                required
                              />
                            </InputGroup>

                            <AvFeedback>This field is invalid</AvFeedback>
                          </AvGroup>

                          <AvGroup className='mb-3'>
                            <Label for='password'>Password</Label>
                            <InputGroup>
                              <InputGroupAddon addonType='prepend'>
                                <span className='input-group-text'>
                                  <Lock className='icon-dual' />
                                </span>
                              </InputGroupAddon>
                              <AvInput
                                type='password'
                                name='password'
                                id='password'
                                placeholder='Enter your password'
                                value={this.state.password}
                                required
                              />
                            </InputGroup>
                            <AvFeedback>This field is invalid</AvFeedback>
                          </AvGroup>

                          <FormGroup className='form-group mb-0 text-center'>
                            <Button
                              color='primary'
                              className='btn-block'
                              disabled={this.state.loading}
                            >
                              {this.state.loading ? 'Logging in...' : 'Log in'}
                            </Button>
                          </FormGroup>
                        </AvForm>
                      </Col>

                      <Col md={6} className='d-none d-md-inline-block'>
                        <div className='auth-page-sidebar'>
                          <div className='overlay'></div>
                          <div className='auth-user-testimonial'>
                            <p className='font-size-24 font-weight-bold text-white mb-1'>
                              We know the right way of trading!
                            </p>
                            <p className='lead'>"The Better Way to Start the Trade"</p>
                          </div>
                        </div>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

export default withFirebase(Login);
