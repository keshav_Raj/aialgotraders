import React from 'react';
import classNames from 'classnames';
import { Card, CardBody, Table, Button } from 'reactstrap';

const TradesTable = ({ trades, strategies }) => {
  const strategiesDetails = {};
  const tradesArray = [];
  if (!trades) {
    return null;
  }
  Object.keys(strategies).forEach((id) => {
    const strategy = strategies[id];
    strategiesDetails[id] = {
      title: strategy.title,
      noOfTrades: 0,
    };
  });
  Object.keys(trades).forEach((id) => {
    const trade = trades[id];
    const tempStrategy = strategiesDetails[[trade['Strategy ID']]];

    const data = {
      status: trade['Order Status'],
      price: trade['Price'],
      stockName: trade['Stock Name'],
      quantity: trade['Quantity'],
      stopLoss: trade['Stoploss'],
      target: trade['Target'],
      trailingStoploss: trade['Trailing Stoploss'],
      triggeredData: trade['Triggered Date'],
      uid: trade['userUid'],
      strategyTitle: tempStrategy ? tempStrategy.title : 'Strategy',
    };
    tradesArray.push(data);
  });

  return (
    <Card>
      <CardBody className='pb-0'>
        <h5 className='card-title mt-0 mb-0 header-title'>Recent Trades</h5>
        <Table hover responsive className='mt-4' striped>
          <thead>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>Stock Name</th>
              <th scope='col'>Strategy</th>
              <th scope='col'>Quantity</th>
              <th scope='col'>Stoploss</th>
              <th scope='col'>Trailing Stoploss</th>
              <th scope='col'>Target</th>
              <th scope='col'>Triggered Date</th>
              <th scope='col'>Price</th>
              <th scope='col'>Status</th>
            </tr>
          </thead>
          <tbody>
            {tradesArray.map((trade, index) => {
              return (
                <tr>
                  <td>{index + 1}</td>
                  <td>{trade.stockName}</td>
                  <td>{trade.strategyTitle}</td>
                  <td>{trade.quantity}</td>
                  <td>{trade.stopLoss}</td>
                  <td>{trade.trailingStoploss}</td>
                  <td>{trade.target}</td>
                  <td>{trade.triggeredData}</td>
                  <td>{trade.price}</td>
                  <td>
                    <span
                      className={classNames('badge  py-1', {
                        ' badge-soft-success': trade.status === 'success',
                        ' badge-soft-warning': trade.status === 'pending',
                      })}
                    >
                      {trade.status}
                    </span>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </CardBody>
    </Card>
  );
};

export default TradesTable;
