// @flow
import React from 'react';
import { Row, Col } from 'reactstrap';

import StatisticsChartWidget from '../../components/StatisticsChartWidget';

const Statistics = ({ backtestHighlights }) => {
  const {
    totalClosedTrades,
    totalWinTrades,
    buy,
    totalBuyWinTrades,
    totalDrawDown,
    sell,
  } = backtestHighlights;

  return (
    <React.Fragment>
      <Row>
        <Col md={6} xl={3} xs={6}>
          <StatisticsChartWidget
            description='Total closed Trades'
            title={totalClosedTrades}
          ></StatisticsChartWidget>
        </Col>

        <Col md={6} xl={3} xs={6}>
          <StatisticsChartWidget
            description='Total win Trades'
            title={totalWinTrades}
          ></StatisticsChartWidget>
        </Col>

        <Col md={6} xl={3} xs={6}>
          <StatisticsChartWidget description='Buy' title={buy}></StatisticsChartWidget>
        </Col>
        <Col md={6} xl={3} xs={6}>
          <StatisticsChartWidget description='Sell' title={sell}></StatisticsChartWidget>
        </Col>

        <Col md={6} xl={3} xs={6}>
          <StatisticsChartWidget
            description='Total buy win trades'
            title={totalBuyWinTrades}
          ></StatisticsChartWidget>
        </Col>
        <Col md={6} xl={3} xs={6}>
          <StatisticsChartWidget
            description='Total draw down'
            title={totalDrawDown}
          ></StatisticsChartWidget>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default Statistics;
