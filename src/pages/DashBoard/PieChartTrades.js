import React from 'react';
import Chart from 'react-apexcharts';
import { Card, CardBody } from 'reactstrap';

const PieChartTrades = ({ trades, strategies }) => {
  const strategiesDetails = {};
  let showChart = false;
  Object.keys(strategies).forEach((id) => {
    const strategy = strategies[id];
    strategiesDetails[id] = {
      title: strategy.title,
      noOfTrades: 0,
    };
  });
  Object.keys(trades).forEach((id) => {
    const trade = trades[id];
    const tempStrategy = strategiesDetails[[trade['Strategy ID']]];
    if (tempStrategy) {
      showChart = true;
      strategiesDetails[[trade['Strategy ID']]] = {
        ...tempStrategy,
        noOfTrades: tempStrategy.noOfTrades + 1,
      };
    }
  });

  const labels = [];
  const noOfTrades = [];

  Object.keys(strategiesDetails).forEach((sId) => {
    const strategy = strategiesDetails[sId];
    labels.push(strategy.title);
    noOfTrades.push(strategy.noOfTrades);
  });

  const options = {
    dataLabels: {
      enabled: false,
    },
    chart: {
      height: 320,
      type: 'pie',
    },
    labels: labels,
    colors: ['#5369f8', '#43d39e', '#f77e53', '#1ce1ac', '#25c2e3', '#ffbe0b'],
    tooltip: {
      theme: 'dark',
      x: { show: false },
    },
    legend: {
      show: true,
      position: 'bottom',
      horizontalAlign: 'center',
      verticalAlign: 'middle',
      floating: false,
      fontSize: '14px',
      offsetX: 0,
      offsetY: -10,
    },
    responsive: [
      {
        breakpoint: 600,
        options: {
          chart: {
            height: 240,
          },
          legend: {
            show: false,
          },
        },
      },
    ],
  };

  const data = noOfTrades;
  if (!showChart) {
    return null;
  }
  return (
    <Card>
      <CardBody className=''>
        <h5 className='card-title mt-0 mb-0 header-title'>Trades By Stratergies</h5>

        <Chart
          options={options}
          series={data}
          type='pie'
          className='apex-charts mb-0 mt-4'
          height={302}
        />
      </CardBody>
    </Card>
  );
};

export default PieChartTrades;
