// @flow
import React from 'react';
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledTooltip,
  UncontrolledPopover,
  PopoverBody,
  Button,
  Badge,
} from 'reactstrap';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import Statistics from './Statistics';
import Error404 from './Error404';

const Project = (props) => {
  const strategy = props.strategy || {};
  const { brokerIds } = props.userData || {};
  const randomLabelColorArray = ['badge-success', 'badge-warning', 'badge-info'];
  const randomLabelColor =
    randomLabelColorArray[Math.floor(Math.random() * randomLabelColorArray.length)];
  const isSubscribed = (strategy.subscribedUsers || []).indexOf(props.userId) > -1;
  const userMapping = props.strategyMappings ? props.strategyMappings[props.strategyId] : {};
  const { demataccount = [] } = userMapping;
  let isDeployed = false;
  for (let i = 0; i < demataccount.length; i++) {
    const dmatData = demataccount[i];
    if (dmatData && dmatData.userUid) {
      if (dmatData.userUid === props.userId) {
        isDeployed = true;
        break;
      }
    }
  }
  const isDmatAccountAvailable = brokerIds.length;
  return (
    <Card>
      <CardBody>
        <div className={classNames('badge', 'float-right', randomLabelColor)}>
          {strategy.categoryType}
        </div>

        <Link to={`/stratergies/${props.strategyId}`}>
          <h5>{strategy.title}</h5>
        </Link>
        <Link to={`/stratergies/${props.strategyId}`}>
          <p className='text-muted mb-4'>
            {strategy.shortDescription}...{' '}
            <span className='d-inline-block mr-1'>
              <h6>view more</h6>
            </span>
          </p>
        </Link>
        <Statistics backtestHighlights={strategy.backtestHighlights} />
      </CardBody>

      <CardBody className='border-top'>
        <Row className='align-items-center'>
          <Col className='col-sm-auto'>
            <ul className='list-inline mb-0'>
              <li className='list-inline-item pr-2'>
                <Link to={`/stratergies/${props.strategyId}`}>
                  <h6 id={`noTasksROI-${props.strategyId}`}>ROI {strategy.roi}</h6>
                  <UncontrolledTooltip placement='top' target={`noTasksROI-${props.strategyId}`}>
                    ROI
                  </UncontrolledTooltip>
                </Link>
              </li>
              <li className='list-inline-item pr-2'>
                <Link to={`/stratergies/${props.strategyId}`}>
                  <h6 id={`noTasks-${props.strategyId}`}>Min Investment {strategy.minCapital}</h6>
                  <UncontrolledTooltip placement='top' target={`noTasks-${props.strategyId}`}>
                    Minimum Investment
                  </UncontrolledTooltip>
                </Link>
              </li>
            </ul>
          </Col>
          <Col className='offset-sm-1'>
            <Row className='align-items-center'>
              <Col xl={4} />
              <Col xl={8}>
                <div
                  className='d-flex mr-3 '
                  style={{ display: 'flex', justifyContent: 'flex-end' }}
                >
                  {!isSubscribed ? (
                    <React.Fragment>
                      <Button
                        className='mr-1'
                        color='success'
                        type='button'
                        id={`subscribe-${props.strategyId}`}
                      >
                        Subscribe
                      </Button>
                      <UncontrolledPopover
                        placement='top'
                        id={`subscribe-${props.strategyId}`}
                        target={`subscribe-${props.strategyId}`}
                        trigger='click'
                      >
                        <PopoverBody>
                          <div className='p-2'>
                            <h6 className='mt-0 header-title'>Confirm subscribe</h6>
                            <button
                              onClick={() => {
                                props.subscribeStratergies(props.strategyId);
                              }}
                              className='btn btn-primary chat-send btn-block'
                            >
                              Yes, Proceed
                            </button>
                          </div>
                        </PopoverBody>
                      </UncontrolledPopover>
                    </React.Fragment>
                  ) : (
                    <button
                      style={{ cursor: 'not-allowed' }}
                      className='btn btn-soft-primary '
                      disabled
                    >
                      Subscribed
                    </button>
                  )}
                  <div className='mr-1' />
                  {!isDmatAccountAvailable ? (
                    <Link to={`/profile`}>
                      <Button color='link' className='btn-rounded'>
                        Please add DMAT info
                      </Button>
                    </Link>
                  ) : (
                    <div>
                      {isDeployed ? (
                        <button
                          style={{ cursor: 'not-allowed' }}
                          className='btn btn-soft-success '
                          disabled
                        >
                          Deployed
                        </button>
                      ) : (
                        <Link to={`/subscribe/${props.strategyId}`}>
                          <button
                            disabled={!isDmatAccountAvailable}
                            type='submit'
                            className='btn btn-info chat-send btn-block'
                          >
                            Deploy
                          </button>
                        </Link>
                      )}
                    </div>
                  )}
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

const AllStratergiesList = ({
  strategies,
  subscribeStratergies,
  userId,
  strategyMappings,
  userData,
}) => {
  const strategyIds = Object.keys(strategies);
  if (strategyIds.length <= 0) {
    return <Error404 title='Empty' message='No strategies found' />;
  }
  return (
    <React.Fragment>
      <Row className='page-title'>
        <Col md={3} xl={6}>
          <h4 className='mb-1 mt-0'>Stratergies</h4>
        </Col>
      </Row>

      <Row>
        {strategyIds.map((strategyId) => {
          const strategy = strategies[strategyId];
          return (
            <Col lg={12} key={'proj-' + strategyId}>
              <Project
                userData={userData}
                strategyMappings={strategyMappings}
                userId={userId}
                strategy={strategy}
                strategyId={strategyId}
                subscribeStratergies={subscribeStratergies}
              />
            </Col>
          );
        })}
      </Row>
    </React.Fragment>
  );
};

export default AllStratergiesList;
