// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import SideBarContainer from './SideBarContainer';
import StratergiesList from './List';
import Loader from '../../components/Loader';

class Stratergies extends Component {
  constructor(props) {
    super(props);
    this.unSubscribeStrategy = this.unSubscribeStrategy.bind(this);
  }

  async unSubscribeStrategy(id) {
    const { firestore, userId, strategies } = this.props;
    const strategy = strategies[id];
    if (!strategy) {
      return;
    }
    const { subscribedUsers } = strategy;
    let tempArray = [...subscribedUsers] || [];
    let index = tempArray.indexOf(userId);
    if (index > -1) {
      tempArray.splice(index, 1);
    }
    const params = {
      subscribedUsers: tempArray,
    };
    const res = await firestore.collection('strategies').doc(id).set(params, { merge: true });
  }

  render() {
    if (!this.props.strategies || !this.props.userData) {
      return <Loader />;
    }
    return (
      <SideBarContainer active_id='mystratergies'>
        <React.Fragment>
          <StratergiesList {...this.props} unSubscribeStrategy={this.unSubscribeStrategy} />
        </React.Fragment>
      </SideBarContainer>
    );
  }
}

const withFireStoreWrapper = compose(
  firestoreConnect(['Trades', 'strategies', 'users']),
  connect((state, props) => {
    const { firebase, firestore } = state;
    const { auth } = firebase;
    return {
      strategies: firestore.data.strategies || null,
      userData: firestore.data.users ? firestore.data.users[auth.uid] || null : null,
      strategyMappings: state.firestore.data.userStrategyMappings || null,
      userId: auth.uid,
    };
  })
);

export default withFireStoreWrapper(Stratergies);
