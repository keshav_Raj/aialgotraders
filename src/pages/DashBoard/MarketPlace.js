// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import SideBarContainer from './SideBarContainer';
import AllStratergiesList from './AllStratergiesList';
import Loader from '../../components/Loader';

class MarketPlace extends Component {
  constructor(props) {
    super(props);
    this.subscribeStratergies = this.subscribeStratergies.bind(this);
  }

  async subscribeStratergies(id) {
    const { firestore, userId, strategies } = this.props;
    const strategy = strategies[id];
    if (!strategy) {
      return;
    }
    const { subscribedUsers } = strategy;
    let tempArray = [...subscribedUsers] || [];
    tempArray.push(userId);
    const params = {
      subscribedUsers: tempArray,
    };
    const res = await firestore.collection('strategies').doc(id).set(params, { merge: true });
  }

  render() {
    if (!this.props.strategies || !this.props.userData) {
      return <Loader />;
    }
    return (
      <SideBarContainer active_id='marketPlace'>
        <React.Fragment>
          <AllStratergiesList {...this.props} subscribeStratergies={this.subscribeStratergies} />
        </React.Fragment>
      </SideBarContainer>
    );
  }
}

const withFireStoreWrapper = compose(
  firestoreConnect(['Trades', 'strategies', 'users']),
  connect((state) => {
    const { firebase, firestore } = state;
    const { auth } = firebase;
    return {
      strategies: firestore.data.strategies || null,
      userData: firestore.data.users ? firestore.data.users[auth.uid] || null : null,
      strategyMappings: state.firestore.data.userStrategyMappings || null,
      userId: auth.uid,
    };
  })
);

export default withFireStoreWrapper(MarketPlace);
