// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import { Row, Col } from 'reactstrap';
import SideBarContainer from './SideBarContainer';
import PieChartTrades from './PieChartTrades';
import TradesTable from './TradesTable';
import Loader from '../../components/Loader';
import Error404 from './Error404';

class UserDashBoard extends Component {
  constructor(props) {
    super(props);
    var oneWeekAgo = new Date();
    oneWeekAgo.setDate(oneWeekAgo.getDate() - 15);

    this.state = {
      filterDate: [oneWeekAgo, new Date()],
    };
  }

  render() {
    if (!this.props.trades || !this.props.strategies) {
      return <Loader />;
    }
    if (Object.keys(this.props.trades) === 0) {
      return <Error404 title='No Trades' message='No Trades started ' />;
    }
    return (
      <SideBarContainer active_id='dashboard'>
        <React.Fragment>
          <div className=''>
            <Row className='page-title align-items-center'>
              <Col sm={4} xl={6}>
                <h4 className='mb-1 mt-0'>Dashboard</h4>
              </Col>
            </Row>
            <Row>
              <Col xl={12}>
                <PieChartTrades trades={this.props.trades} strategies={this.props.strategies} />
              </Col>
              <Col xl={12}>
                <TradesTable trades={this.props.trades} strategies={this.props.strategies} />
              </Col>
            </Row>
          </div>
        </React.Fragment>
      </SideBarContainer>
    );
  }
}

const withFireStoreWrapper = compose(
  firestoreConnect(['Trades', 'strategies']),
  connect((state) => ({
    trades: state.firestore.data.Trades || null,
    strategies: state.firestore.data.strategies || null,
  }))
);

export default withFireStoreWrapper(UserDashBoard);
