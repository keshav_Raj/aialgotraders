import React from 'react';
import { Card, CardBody, Input } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { useFirestoreConnect } from 'react-redux-firebase';
import { useSelector } from 'react-redux';
import Loader from '../../components/Loader';

const sizePerPageRenderer = ({ options, currSizePerPage, onSizePerPageChange }) => (
  <React.Fragment>
    <label className='d-inline mr-1'>Show</label>
    <Input
      type='select'
      name='select'
      id='no-entries'
      className='custom-select custom-select-sm d-inline col-1'
      defaultValue={currSizePerPage}
      onChange={(e) => onSizePerPageChange(e.target.value)}
    >
      {options.map((option, idx) => {
        return <option key={idx}>{option.text}</option>;
      })}
    </Input>
    <label className='d-inline ml-1'>entries</label>
  </React.Fragment>
);

const TableContainer = ({ strategyId, strategy }) => {
  const { backTestValuesToShow = [] } = strategy;
  useFirestoreConnect([
    {
      collection: `strategies/${strategyId}/backTestResults`,
      storeAs: 'backTestResultsData',
    },
  ]);
  const data = useSelector(({ firestore: { data } }) => data.backTestResultsData || null);
  if (!data || Object.keys(data).length === 0) {
    return <Loader />;
  }

  const tempObject = Object.keys(data)[0];
  const keys = Object.keys(data[tempObject]);
  const columns = [];
  const records = [];

  Object.keys(data).forEach((tempDataKey) => {
    records.push(data[tempDataKey]);
  });
  backTestValuesToShow.forEach((keyData) => {
    if (keyData && keyData.display) {
      columns.push({
        dataField: keyData.key,
        text: keyData.name,
        sort: true,
      });
    }
  });

  const defaultSorted = [
    {
      dataField: 'id',
      order: 'asc',
    },
  ];

  const customTotal = (from, to, size) => (
    <span className='react-bootstrap-table-pagination-total ml-4'>
      Showing {from} to {to} of {size} Results
    </span>
  );

  const paginationOptions = {
    paginationSize: 5,
    pageStartIndex: 1,
    firstPageText: 'First',
    prePageText: 'Back',
    nextPageText: 'Next',
    lastPageText: 'Last',
    nextPageTitle: 'First page',
    prePageTitle: 'Pre page',
    firstPageTitle: 'Next page',
    lastPageTitle: 'Last page',
    showTotal: true,
    paginationTotalRenderer: customTotal,
    sizePerPageRenderer: sizePerPageRenderer,
    sizePerPageList: [
      {
        text: '5',
        value: 5,
      },
      {
        text: '10',
        value: 10,
      },
      {
        text: '25',
        value: 25,
      },
    ],
  };

  return (
    <Card>
      <CardBody>
        <BootstrapTable
          bootstrap4
          keyField='id'
          data={records}
          columns={columns}
          defaultSorted={defaultSorted}
          pagination={paginationFactory(paginationOptions)}
          wrapperClasses='table-responsive'
        />
      </CardBody>
    </Card>
  );
};

export default TableContainer;
