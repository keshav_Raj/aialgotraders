import React from 'react';
import { createBrowserHistory } from 'history';
import { Router, Route, Switch } from 'react-router-dom';
import firebase from 'firebase/app';
import { useSelector } from 'react-redux';
import { useFirestoreConnect } from 'react-redux-firebase';
import { useHistory } from 'react-router-dom';
import { firestoreConnect } from 'react-redux-firebase';
import Login from '../pages/auth/Login';
import UserDashBoard from '../pages/DashBoard/UserDashBoard';
import Stratergies from '../pages/DashBoard/Stratergies';
import MarketPlace from '../pages/DashBoard/MarketPlace';
import StratergiesDetail from '../pages/Detail';
import SubscribedStratergyDetails from '../pages/Detail/SubscribedStratergyDetails';
import SubscribeStratergies from '../pages/Subscribe/SubscribeStratergies';
import Profile from '../pages/profile';

const hist = createBrowserHistory();

const Navigators = () => {
  const [isUserSignedIn, setIsUserSignedIn] = React.useState(false);
  const [isAdmin, setIsAdmin] = React.useState(false);
  const userInfo = firebase.auth().currentUser;
  useFirestoreConnect([
    {
      collection: 'users',
      doc: userInfo ? userInfo.uid : null,
    },
  ]);
  const userProfile = useSelector(
    ({ firestore: { data } }) => data.users && data.users[userInfo ? userInfo.uid : null]
  );

  const changeUserState = (loggedIn) => {
    if (loggedIn && !isUserSignedIn) {
      setIsUserSignedIn(true);
    }
    if (!loggedIn && isUserSignedIn) {
      setIsUserSignedIn(false);
    }
  };
  if (userInfo) {
    changeUserState(true);
  }

  const changeUserRole = (isUserAdmin) => {
    if (isUserAdmin) {
      setIsAdmin(true);
    }
  };

  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      changeUserState(true);
      if (userProfile && userProfile.userRole && userProfile.userRole === '1') {
        changeUserRole(true);
      }
    } else {
      changeUserState(false);
    }
  });

  const Test = () => <>Admin</>;
  const TestUSer = () => <>user</>;

  return (
    <Router history={hist}>
      {!isUserSignedIn ? (
        <>
          <Switch>
            <Route path='/' component={Login} />
          </Switch>
        </>
      ) : isAdmin ? (
        <>
          <Switch>
            <Route path='/' component={Test} />
          </Switch>
        </>
      ) : (
        <>
          <Switch>
            <Route path='/stratergies/:id' component={StratergiesDetail} />
            <Route path='/my-stratergies/:id' component={SubscribedStratergyDetails} />
            <Route path='/profile' component={Profile} />
            <Route path='/stratergies' component={Stratergies} />
            <Route path='/subscribe/:id' component={SubscribeStratergies} />
            <Route path='/marketPlace' component={MarketPlace} />
            <Route path='/' component={UserDashBoard} />
          </Switch>
        </>
      )}
    </Router>
  );
};

export default firestoreConnect()(Navigators);
