function manageBodyClass(cssClass, action = 'toggle') {
  switch (action) {
    case 'add':
      if (document.body) document.body.classList.add(cssClass);
      break;
    case 'remove':
      if (document.body) document.body.classList.remove(cssClass);
      break;
    default:
      if (document.body) document.body.classList.toggle(cssClass);
      break;
  }
  return true;
}

function* changeLeftSidebarType({ payload: type }) {
  try {
    switch (type) {
      case layoutConstants.LEFT_SIDEBAR_TYPE_CONDENSED:
        manageBodyClass('left-side-menu-condensed', 'add');
        break;
      case layoutConstants.LEFT_SIDEBAR_TYPE_SCROLLABLE:
        manageBodyClass('left-side-menu-condensed', 'remove');
        manageBodyClass('scrollable-layout', 'add');
        break;
      default:
        manageBodyClass('left-side-menu-condensed', 'remove');
        manageBodyClass('scrollable-layout', 'remove');
        break;
    }
  } catch (error) {}
}

export { manageBodyClass, changeLeftSidebarType };
