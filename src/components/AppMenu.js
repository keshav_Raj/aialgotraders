import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import classNames from 'classnames';
import MetisMenu from 'metismenujs/dist/metismenujs';

const MenuItemWithChildren = ({
  item,
  linkClassNames,
  subMenuClassNames,
  activatedMenuItemIds,
}) => {
  const Icon = item.icon || null;
  return (
    <li
      className={classNames('side-nav-item', {
        'active mm-active': activatedMenuItemIds.indexOf(item.id) >= 0,
      })}
    >
      <Link
        to='/'
        className={classNames('side-sub-nav-link', linkClassNames)}
        aria-expanded={activatedMenuItemIds.indexOf(item.id) >= 0}
      >
        {item.icon && <Icon />}
        {item.badge && (
          <span className={`badge badge-${item.badge.variant} float-right`}>{item.badge.text}</span>
        )}
        <span> {item.name} </span>
        <span className='menu-arrow'></span>
      </Link>

      <ul
        className={classNames(subMenuClassNames, {
          'mm-collapse mm-show': activatedMenuItemIds.indexOf(item.id) >= 0,
        })}
        aria-expanded={activatedMenuItemIds.indexOf(item.id) >= 0}
      >
        {item.children.map((child, i) => {
          return (
            <React.Fragment key={i}>
              {child.children ? (
                <MenuItemWithChildren
                  item={child}
                  linkClassNames={classNames({
                    active: activatedMenuItemIds.indexOf(child.id) >= 0,
                  })}
                  activatedMenuItemIds={activatedMenuItemIds}
                  subMenuClassNames='side-nav-third-level'
                />
              ) : (
                <MenuItem
                  item={child}
                  className={classNames({ active: activatedMenuItemIds.indexOf(child.id) >= 0 })}
                  linkClassName={classNames({
                    active: activatedMenuItemIds.indexOf(child.id) >= 0,
                  })}
                />
              )}
            </React.Fragment>
          );
        })}
      </ul>
    </li>
  );
};

const MenuItem = ({ item, className, linkClassName }) => {
  return (
    <li className={classNames('side-nav-item', className)}>
      <MenuItemLink item={item} className={linkClassName} />
    </li>
  );
};

const MenuItemLink = ({ item, className }) => {
  const Icon = item.icon || null;
  return (
    <Link
      to={item.path}
      className={classNames('side-nav-link-ref', 'side-sub-nav-link', className)}
    >
      {item.icon && <Icon />}
      {item.badge && (
        <span className={`font-size-12 badge badge-${item.badge.variant} float-right`}>
          {item.badge.text}
        </span>
      )}
      <span> {item.name} </span>
    </Link>
  );
};

/**
 * Renders the application menu
 */

class AppMenu extends Component {
  render() {
    const isHorizontal = this.props.mode === 'horizontal';
    const menuItems = [
      {
        path: '/',
        name: 'Dashboard',
        id: 'dashboard',
      },
      {
        path: '/profile',
        name: 'My Profile',
        id: 'myprofile',
      },
      {
        path: '/marketPlace',
        name: 'Marketplace',
        id: 'marketPlace',
      },
      {
        path: '/stratergies',
        name: 'My Strategies',
        id: 'mystratergies',
      },
    ];
    return (
      <React.Fragment>
        <ul className='metismenu' id='menu-bar'>
          {menuItems.map((item, i) => {
            return (
              <React.Fragment key={item.id}>
                {item.header && !isHorizontal && (
                  <li className='menu-title' key={i + '-el'}>
                    Test
                  </li>
                )}
                <MenuItem
                  item={item}
                  className={classNames({ 'mm-active': this.props.active_id === item.id })}
                  linkClassName='side-nav-link'
                />
              </React.Fragment>
            );
          })}
        </ul>
      </React.Fragment>
    );
  }
}

export default withRouter(AppMenu);
